##########################
##########################
# Tianmu Zhang, tzhang4@buffalo.edu
# Department of Material Design and Innovation
# https://engineering.buffalo.edu/materials-design-innovation.html
# University at Buffalo, The State University of New York
#
# Read the output file from Crystal Explorer, calculate the persistent homology
# based on the value on the surface points.
import HSXPLR as HSXPLR
import dionysus as dos
import matplotlib.pyplot as plt
import numpy as np
import itertools

bar_clrs = itertools.cycle(['g', 'b', 'r'])
# fn ='CAYDEN_MedQuality'
# fn ='CAYDEN_HighQuality'
# surf_val = 'd_norm'
def HS_PH(file_name, surf_val='d_norm'):
    if isinstance(file_name, str):
        h_surf = HSXPLR.HSXPLR(file_name+'.cxs')
    elif isinstance(file_name, HSXPLR.HSXPLR):
        h_surf = file_name
    else:
        raise TypeError('Input is neither a HSXPLR object or a file name')
    vertices = h_surf.HSsf_vtxidx
    if surf_val == 'd_norm':
        sclrs = h_surf.VTX_dnorm
    elif surf_val == 'd_i':
        sclrs = h_surf.VTX_di
    elif surf_val == 'd_e':
        sclrs = h_surf.VTX_de
    else:
        raise TypeError('Using '+surf_val+' to color the surface is not\
                        currently supported.')
    # 0 simplice, directly attach selected surface values.
    simplices0 = zip([[vtt] for vtt in range(h_surf.HSsf_ptnum)], sclrs)
    # 1 simplices
    simplices1 = []
    simplices1sl = []
    for vtt in vertices:
        if set([vtt[0], vtt[1]]) not in simplices1sl:
            simplices1.append(([vtt[0], vtt[1]], max(sclrs[vtt[0]],
                                sclrs[vtt[1]])))
            simplices1sl.append(set([vtt[0], vtt[1]]))
        if set([vtt[0], vtt[2]]) not in simplices1sl:
            simplices1.append(([vtt[0], vtt[2]], max(sclrs[vtt[0]],
                                sclrs[vtt[2]])))
            simplices1sl.append(set([vtt[0], vtt[2]]))
        if set([vtt[1], vtt[2]]) not in simplices1sl:
            simplices1.append(([vtt[1], vtt[2]], max(sclrs[vtt[1]],
                                sclrs[vtt[2]])))
            simplices1sl.append(set([vtt[1], vtt[2]]))
    # 2 simplices
    simplices2 = []
    simplices2ls = []
    for vtt in vertices:
        if set([vtt[0], vtt[1], vtt[2]]) not in simplices2ls:
            simplices2.append(([vtt[0], vtt[1], vtt[2]],
                          max(sclrs[vtt[0]], sclrs[vtt[1]], sclrs[vtt[2]] )))
            simplices2ls.append([vtt[0], vtt[1], vtt[2]])
    # Putting all simplices together
    simplices123 = simplices0 + simplices1 + simplices2
    # Filtration
    flt = dos.Filtration(simplices123)
    flt.sort()
    m = dos.homology_persistence(flt)
    dgms = dos.init_diagrams(m, flt)
    return dgms

def plot_HSBCD(dgms, dim=None, fig_pop=None, max_flt_val=10, crt_flt_val=None):
    if fig_pop == None:
        fig_bcd = plt.figure()
    else:
        fig_bcd = fig_pop
    ax_bcd = fig_bcd.add_axes([.01,.05, .98, .9])

    if dim == None:
        dim_ls = range(len(dgms))
    elif dim != None:
        if isinstance(dim, list):
            dim_ls = dim
        elif isinstance(dim, (int, long)):
            dim_ls = [dim]
        else:
            raise TypeError('The dimension of the barcode to plot should be a int or list.')

    barv = 0
    bd_values = []
    for dimm in dim_ls:
        bfclr = bar_clrs.next()
        for bd in dgms[dimm]:
            if bd.death == np.inf:
                ax_bcd.broken_barh([(bd.birth, max_flt_val-bd.birth)], (barv, .3),
                    facecolors=bfclr, lw=0,)
                bd_values.append(bd.birth)
            else:
                ax_bcd.broken_barh([(bd.birth, bd.death-bd.birth)], (barv, .3),
                    facecolors=bfclr, lw=0, )
                bd_values.append(bd.birth)
                bd_values.append(bd.death)
            barv+=1
    # ax_bcd.set_xlim(
    #             left=min(bd_values)-(max(bd_values)-min(bd_values))/10.0,
    #             right=max(bd_values)+(max(bd_values)-min(bd_values))/5.0
    #                 )
    ax_bcd.set_xlim(
                left=-.8, # for dim 1 to match the colorbar purose only
                right=max(bd_values)+(max(bd_values)-min(bd_values))/5.0
                    )
    ax_bcd.set_yticks([])
    fig_bcd.savefig('./bcd_dm1.png') # for saving figure of barcode only.
    return fig_bcd
    # fig_bcd.show()

def plot_HSBCD_overlay(dgms, dim=None, fig_pop=None, max_flt_val=10, crt_flt_val=None):
    if fig_pop == None:
        fig_bcd = plt.figure()
    else:
        fig_bcd = fig_pop
    ax_bcd = fig_bcd.add_axes([.01,.05, .98, .9])

    if dim == None:
        dim_ls = range(len(dgms))
    elif dim != None:
        if isinstance(dim, list):
            dim_ls = dim
        elif isinstance(dim, (int, long)):
            dim_ls = [dim]
        else:
            raise TypeError('The dimension of the barcode to plot should be a int or list.')
    bar_offset = 0
    for dgmm in dgms:
        barv = 0 + bar_offset
        bd_values = []
        for dimm in dim_ls:
            bfclr = bar_clrs.next()
            for bd in dgms[dimm]:
                if bd.death == np.inf:
                    ax_bcd.broken_barh([(bd.birth, max_flt_val-bd.birth)], (barv, .3),
                        facecolors=bfclr, lw=0,)
                    bd_values.append(bd.birth)
                else:
                    ax_bcd.broken_barh([(bd.birth, bd.death-bd.birth)], (barv, .3),
                        facecolors=bfclr, lw=0, )
                    bd_values.append(bd.birth)
                    bd_values.append(bd.death)
                barv+=1
        bar_offset +=.5
        # ax_bcd.set_xlim(
        #             left=min(bd_values)-(max(bd_values)-min(bd_values))/10.0,
        #             right=max(bd_values)+(max(bd_values)-min(bd_values))/5.0
        #                 )
    ax_bcd.set_xlim(
                left=-.8, # for dim 1 to match the colorbar purose only
                right=max(bd_values)+(max(bd_values)-min(bd_values))/5.0
                    )
    ax_bcd.set_yticks([])
    fig_bcd.savefig('./bcd_dm1.png') # for saving figure of barcode only.
    return fig_bcd
        # fig_bcd.show()

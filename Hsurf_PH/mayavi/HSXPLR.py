import fnmatch
    
class HSXPLR:
    '''
    A class to read the output file of Hirshfeld calculation from CrystalXplore.
    '''
    def __init__(self, file_name):
        self.HSsf_vtx = []
        self.HSsf_vtxidx = []
        self.VTX_dide = []
        self.VTX_di = []
        self.VTX_de = []
        self.VTX_didenorm = []
        self.VTX_dinorm = []
        self.VTX_denorm = []
        self.VTX_dnorm = []
        with open(file_name, 'r') as fp:
            self.dat = fp.readlines()

        # Read the 3D surface coordinates, currently only the orignal coordinates are read.
        # Find the index of the line "begin surface  Hirshfeld_surface"
#         HSsf_begidx = self.dat.index('begin surface  Hirshfeld_surface\n')
        self.HSsf_begidx = [ii for ii, xx in enumerate(self.dat) if xx == 'begin surface  Hirshfeld_surface\n']
        if len(self.HSsf_begidx) == 1:
            self.HSsf_begidx = self.HSsf_begidx[0]
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: HSsf_begidx.')
        # Find the index of the line "begin vertices ***". *** is the number vertices.
        # The element of the list is the number of the vertices.
#         self.HSsf_vtx_begidx = self.dat.index(fnmatch.filter(self.dat, 'begin vertices *')[0])
        self.HSsf_vtx_begidx = \
        [xx for ii, xx in enumerate(fnmatch.filter(self.dat, 'begin vertices *'))]
        if len(self.HSsf_vtx_begidx) == 1:
            self.HSsf_vtx_begidx = self.dat.index(fnmatch.filter(self.dat, 'begin vertices *')[0])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: HSsf_vtx_begidx.')
        if len(fnmatch.filter(self.dat, 'begin vertices *')) == 1:
            self.HSsf_num_vtx = int(fnmatch.filter(self.dat, 'begin vertices *')[0].split()[-1])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: HSsf_num_vtx.')
        if self.dat[self.HSsf_vtx_begidx+self.HSsf_num_vtx+1] == 'end vertices\n':
            for vtxc in self.dat[self.HSsf_vtx_begidx+1:self.HSsf_vtx_begidx+self.HSsf_num_vtx+1]:
                self.HSsf_vtx.append([float(vtxcc) for vtxcc in vtxc.split()])
        else:
            raise ValueError('The number of vertice in the file did not\
                            match the atcual number vertice in the file: original vertices coordinates.')
        ###############
        self.HSsf_vtxidx_begidx = self.dat.index(fnmatch.filter(self.dat, 'begin indices *')[0])
        self.HSsf_num_vtxidx = int(fnmatch.filter(self.dat, 'begin indices *')[0].split()[-1])
        if self.dat[self.HSsf_vtxidx_begidx+self.HSsf_num_vtxidx+1] == 'end indices\n':
            for vtxidxc in self.dat[self.HSsf_vtxidx_begidx+1:self.HSsf_vtxidx_begidx+self.HSsf_num_vtxidx+1]:
                self.HSsf_vtxidx.append([int(vtxcc) for vtxcc in vtxidxc.split()])
        else:
            raise ValueError('The number of vertice indices in the file did not\
                            match the atcual number vertice in the file: original vertices coordinates.')
        ############################################################
        ############################################################
        # Read the d_i and d_e for all the vertices on the 3D surface
        self.VTXppt_begidx = [ii for ii, xx in enumerate(self.dat) if xx == 'begin vertex_properties\n']
        if len(self.VTXppt_begidx) == 1:
            self.VTXppt_begidx = self.VTXppt_begidx[0]
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: VTXppt_begidx.')
#         self.VTXppt_begidx = self.dat.index('begin vertex_properties\n')
        if len(fnmatch.filter(self.dat,'begin d_i *')) == 1:
            self.VTXdi_begidx = self.dat.index(fnmatch.filter(self.dat,'begin d_i *')[0])
            self.VTXdi_num = int(fnmatch.filter(self.dat, 'begin d_i *')[0].split()[-1])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: VTXdi_begidx')
        if len(fnmatch.filter(self.dat,'begin d_e *')) == 1:
            self.VTXde_begidx = self.dat.index(fnmatch.filter(self.dat,'begin d_e *')[0])
            self.VTXde_num = int(fnmatch.filter(self.dat, 'begin d_e *')[0].split()[-1])
        if (self.dat[self.VTXdi_begidx+self.VTXdi_num+1] == 'end d_i\n') and\
        (self.dat[self.VTXde_begidx+self.VTXde_num+1] == 'end d_e\n'):
            for di_t, de_t in zip(self.dat[self.VTXdi_begidx+1:self.VTXdi_begidx+self.VTXdi_num+1],\
                                  self.dat[self.VTXde_begidx+1:self.VTXde_begidx+self.VTXde_num+1]):
#                 self.VTX_dide.append((float(di_t), float(de_t)))
                self.VTX_dide.append([float(di_t), float(de_t)])
                self.VTX_di.append(float(di_t))
                self.VTX_de.append(float(de_t))
        else:
            raise ValueError('The number of vertice in the file did not\
                            match the atcual number vertice in the file: di/de values.')

        if len(fnmatch.filter(self.dat,'begin d_norm_i *')) == 1:
            self.VTXdinorm_begidx = self.dat.index(fnmatch.filter(self.dat,'begin d_norm_i *')[0])
            self.VTXdinorm_num = int(fnmatch.filter(self.dat, 'begin d_norm_i *')[0].split()[-1])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: VTXdinorm_begidx')
        if len(fnmatch.filter(self.dat,'begin d_norm_e *')) == 1:
            self.VTXdenorm_begidx = self.dat.index(fnmatch.filter(self.dat,'begin d_norm_e *')[0])
            self.VTXdenorm_num = int(fnmatch.filter(self.dat, 'begin d_norm_e *')[0].split()[-1])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: VTXdenorm_begidx')

        if (self.dat[self.VTXdinorm_begidx+self.VTXdinorm_num+1] == 'end d_norm_i\n') and\
        (self.dat[self.VTXdenorm_begidx+self.VTXdenorm_num+1] == 'end d_norm_e\n'):
            for di_t, de_t in zip(self.dat[self.VTXdinorm_begidx+1:self.VTXdinorm_begidx+self.VTXdinorm_num+1],\
                                  self.dat[self.VTXdenorm_begidx+1:self.VTXdenorm_begidx+self.VTXdenorm_num+1]):
                self.VTX_didenorm.append((float(di_t), float(de_t)))
                self.VTX_dinorm.append(float(di_t))
                self.VTX_denorm.append(float(de_t))
        else:
            raise ValueError('The number of vertice in the file did not\
                            match the atcual number vertice in the file: d_norm_i/d_norm_e values.')

        if len(fnmatch.filter(self.dat,'begin d_norm *')) == 1:
            self.VTXdnorm_begidx = self.dat.index(fnmatch.filter(self.dat,'begin d_norm *')[0])
            self.VTXdnorm_num = int(fnmatch.filter(self.dat, 'begin d_norm *')[0].split()[-1])
        else:
            raise ValueError('It seems there are more than one indices set for Hirshfeld Surface: VTXdnorm_begidx')
        if (self.dat[self.VTXdnorm_begidx+self.VTXdnorm_num+1] == 'end d_norm\n'):
            for dn_t in self.dat[self.VTXdnorm_begidx+1:self.VTXdnorm_begidx+self.VTXdnorm_num+1]:
                self.VTX_dnorm.append(float(dn_t))
        else:
            raise ValueError('The number of vertice in the file did not\
                            match the atcual number vertice in the file: d_norm values.')

        self.VTX_dimax = max(self.VTX_di)
        self.VTX_dimin = min(self.VTX_di)
        self.VTX_demax = max(self.VTX_de)
        self.VTX_demin = min(self.VTX_de)
        self.VTX_dnmax = max(self.VTX_dnorm)
        self.VTX_dnmin = min(self.VTX_dnorm)

        if self.HSsf_num_vtx == self.VTXdi_num == self.VTXde_num:
            self.HSsf_ptnum = self.VTXdi_num
        else:
            raise ValueError('The number of vertice did not match the number of\
                            d_i or d_e.')

import matplotlib.pyplot as plt
import matplotlib.animation as manimation
import plot_HS3D2
import numpy as np
from mayavi import mlab
# mlab.options.offscreen=True
import sys

fn ='CAYDEN_MedQuality'
# fn ='CAYDEN_HighQuality'
# thrshld_list = np.arange(-.7,2.2,.01)
thrshld_list = np.arange(-.7,2.2,.2)

FFMpegWriter = manimation.writers['ffmpeg']
metadata = dict(title='Hirshfeld Surface Movie Test', artist='Tianmu',
                comment='')
writer = FFMpegWriter(fps=15, metadata=metadata)
figpy1 = plt.figure()
axpy1 = figpy1.add_subplot(1,1,1)

figmy1 = mlab.figure(bgcolor=(1,1,1))
axlimits = [3,16, 3,11, 0,9]
# mlab.axes(figure=figmy1, ranges=axlimits)

with writer.saving(figpy1, 'HS_test1.mp4', 400):
    for thrshld in thrshld_list:
        sys.stdout.write('writing movie: '+str(thrshld))
        sys.stdout.write('\r')
        sys.stdout.flush()

        plot_HS3D.plot_HS3D(fn, msk_val=thrshld, fig_pop=figmy1, extent=axlimits)
        # plot_HS3D.plot_HS3D(fn, msk_val=thrshld, fig_pop=figmy1, )
        im_arr = mlab.screenshot()
        # mlab.close()
        axpy1.imshow(im_arr)
        figpy1.tight_layout()
        writer.grab_frame()

import numpy as np
import mayavi.mlab as mlab
import  moviepy.editor as mpy

duration= 2 # duration of the animation in seconds (it will loop)

# MAKE A FIGURE WITH MAYAVI

fig_myv = mlab.figure(size=(420,421), bgcolor=(1,1,1))
# fig_myv = mlab.figure( bgcolor=(1,1,1))
X, Y = np.linspace(-2,2,200), np.linspace(-2,2,200)
XX, YY = np.meshgrid(X,Y)
ZZ = lambda d: np.sinc(XX**2+YY**2)+np.sin(XX+d)

# ANIMATE THE FIGURE WITH MOVIEPY, WRITE AN ANIMATED GIF

def make_frame(t):
    mlab.clf() # clear the figure (to reset the colors)
    mlab.mesh(YY,XX,ZZ(2*np.pi*t/duration), figure=fig_myv)
    # return mlab.screenshot( antialiased=True)
    return mlab.screenshot(figure=fig_myv, antialiased=True)

animation = mpy.VideoClip(make_frame, duration=duration)
# animation.write_gif("sinc.gif", fps=20)
animation.write_videofile("sinc.mp4", fps=20,
        # ffmpeg_params=["-profile:v","baseline", "-level","3.0","-pix_fmt", "yuv420p"]
        )
# mlab.savefig('./test.png')
# animation.to_videofile("sinc.mp4", fps=24)

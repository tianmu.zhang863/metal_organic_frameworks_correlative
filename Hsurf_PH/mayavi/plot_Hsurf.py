import os
import numpy as np
from mayavi import mlab
import plot_HS3D2

# mlab.options.offscreen=True

# structures = [f for f in os.listdir('./HS_data') if f.endswith(".cxs")]
# structures = [f.split(".")[0] for f in structures]

for mof_id in structures:
    # h_surf = plot_HS3D2.read_XPLR('./HS_data/'+mof_id) # read file from CrystalExplorer
    h_surf = plot_HS3D2.read_XPLR('./../cxs_med/'+mof_id) # read file from CrystalExplorer
    fig_myv = mlab.figure(size=(620,620), bgcolor=(1,1,1))
    hs, vw = plot_HS3D2.plot_HS3D(h_surf, msk_val=5.0, fig_pop=fig_myv)
    # mlab.savefig(prvsk+'.png')

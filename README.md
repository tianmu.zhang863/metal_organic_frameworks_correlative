# This repo contains the code for the mapping of Metal-organic-frameworks. Part of the work (geometric fingerprint of Hirshfeld surfaces) has been appeared in the paper (in MSDE_perper..):
## Correlative analysis of metal organic framework structures through manifold learning of Hirshfeld surfaces  
Xiaozhou Shen, Tianmu Zhang, Scott Broderick and Krishna Rajan
https://pubs.rsc.org/en/content/articlelanding/2018/me/c8me00014j#!divAbstract

# The other half is topological fingerprint, in Hsurf_PH
### Example of 3D Mapping of MOF structures
![](/MSDE_paper_0218/HS_dataset_final/rev_201807/figs_final/isomv508cm3_alt0.mp4)
### Example of how a barcode is generated from a Hirshfeld Surface  
- The color on the surface is a selected property of the surface
- By continuously changing the threshold different parts of the surface is added.
- Barcode "records" the change of the topological features in this process.
![](/Hsurf_PH/HS_flt_v2_2.mp4/)
